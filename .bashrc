# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions

#
# ADDED HELLASTORM
#

PS1='\n\[\e[01;36m\]\u \[\e[0m\]on \[\e[01;33m\]\h \[\e[0m\]in \[\e[01;34m\]\w\[\e[0m\]\n$ '

export HISTSIZE=2000
export HISTFILESIZE=2000
export HISTIGNORE="&:[ ]*:exit:ls:la:ll:lll:history"
# don't put duplicate lines or lines starting with space in the history.
HISTCONTROL=ignoreboth

alias ls='ls -x --color=auto --group-directories-first'
alias la='ls -Ax --color=auto --group-directories-first'
alias ll='ls -l --color=auto --group-directories-first'
alias lll='ls -lA --color=auto --group-directories-first'
alias rsyncp='rsync -avzh --info=progress2 --info=name0 --stats'

export PATH=$PATH:/usr/local/go/bin

export loc_linux_kernel=/usr/src/kernels/$(basename $(uname -r) -generic)
export loc_linux_kernel_generic=/usr/src/kernels/$(uname -r)

# END HELLASTORM
