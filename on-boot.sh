#!/bin/sh

# Allocate a pool of 2MiB huge pages
sysctl -w vm.nr_hugepages=128

# Allocate a pool of 1GiB huge pages
echo 8 | sudo tee /sys/kernel/mm/hugepages/hugepages-1048576kB/nr_hugepages
