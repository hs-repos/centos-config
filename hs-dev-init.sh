GOPATH=$HOME/AppDev/Go
export GOPATH
GOPRIVATE=bitbucket.org/hs-repos
export GOPRIVATE

SSH_ENV="$HOME/.ssh/env"

function start_agent {
    agent=`pgrep ssh-agent -u $USER` # get only your agents
    if [[ "$agent" == "" || ! -e ~/.ssh/.agent_env ]]; then
        kill $agent running
        rm ~/.ssh/.agent-env

        echo "Initializing new SSH agent..."
        /usr/bin/ssh-agent | sed 's/^echo/#echo/' > "${SSH_ENV}"
        echo succeeded
        chmod 600 "${SSH_ENV}"
        . "${SSH_ENV}" > /dev/null

        /usr/bin/ssh-add;

        echo 'export SSH_AUTH_SOCK'=$SSH_AUTH_SOCK >> ~/.ssh/.agent_env
        echo 'export SSH_AGENT_PID'=$SSH_AGENT_PID >> ~/.ssh/.agent_env
    else
        echo "Agent already running..."
    fi
}

#Source SSH settings, if applicable

if [ -f "${SSH_ENV}" ]; then
    . "${SSH_ENV}" > /dev/null
    #ps ${SSH_AGENT_PID} doesn't work under cywgin
    ps -ef | grep ${SSH_AGENT_PID} | grep ssh-agent$ > /dev/null || {
        start_agent;
    }
else
    start_agent;
fi
